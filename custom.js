let url = new URL("/cpuWs", window.location.href);
url.protocol = url.protocol.replace("http", "ws");

let cpuWs = new WebSocket(url.href);

let ms_changed = 0;
let ms = 0;

let add_loop = null;
let sub_loop = null;
let calc_timeout = null;

function add() {
    let new_ms = ms + ms_changed;

    if (ms_changed == 200) {
        clearInterval(add_loop);
        add_loop = setInterval(add_more,  250);
        return;
    }

    if (new_ms < 10000) {
        ms_changed = parseInt(ms_changed) + 10;
        document.getElementById("count").innerHTML = ms + ms_changed;
    }
};

function sub() {
    let new_ms = ms + ms_changed;

    if (ms_changed == -200) {
        clearInterval(sub_loop);
        sub_loop = setInterval(sub_more,  250);
        return;
    }

    if (new_ms > 100) {
        ms_changed = parseInt(ms_changed) - 10;
        document.getElementById("count").innerHTML = ms + ms_changed;
    }
};

function add_more() {
    let new_ms = ms + ms_changed;

    if (ms_changed == 2000) {
        clearInterval(add_loop);
        add_loop = setInterval(add_more,  100);
    }

    if (new_ms < 10000) {
        ms_changed = parseInt(ms_changed) + 100;
        document.getElementById("count").innerHTML = ms + ms_changed;
    }
};

function sub_more() {
    let new_ms = ms + ms_changed;

    if (ms_changed == -2000) {
        clearInterval(sub_loop);
        sub_loop = setInterval(sub_more,  100);
    }

    if (new_ms > 100) {
        ms_changed = parseInt(ms_changed) - 100;
        document.getElementById("count").innerHTML = ms + ms_changed;
    }
}

function calc() {
    if (ms_changed == 0) {
        return;
    }
    let new_ms = ms + ms_changed;
    console.log(new_ms);
    console.log(ms);
    if (new_ms <= 10000 && new_ms >= 100) {
        let json = '{"ms":' + new_ms + '}';
        cpuWs.send(json);
    }
    ms_changed = 0;
    ms =  new_ms;
    if (document.getElementById("count") != new_ms) {
        document.getElementById("count").innerHTML = new_ms;
    }
};

$('#oadd').on('mousedown', function () {
    clearTimeout(calc_timeout);
    add();
    add_loop = setInterval(add,  100);
}).on('mouseup', function () {
    clearInterval(add_loop);
    calc_timeout = setTimeout(calc, 1000);
});

$('#osub').on('mousedown', function () {
    clearTimeout(calc_timeout);
    sub();
    sub_loop = setInterval(sub, 100);
}).on('mouseup', function () {
    clearInterval(sub_loop);
    calc_timeout = setTimeout(calc, 1000);
});

let table = document.getElementById("tb");

let xx = 0;

function json_update(jso) {
    let cpu = jso["cores"];
    let mem_free = jso["mem_free"];
    let mem_total = jso["mem_total"];
    let cpu_avg = jso["cpu_avg"];
    let up = jso["net_up"];
    let dn = jso["net_down"];
    let die_tmp = jso["die_tmp"];
    let fan_tmp = jso["fan_tmp"];

    if (up < 1000) {
        document.getElementById("net_up").innerHTML = jso["net_up"] + " bps";
    } else if (up < 1000000) {
        document.getElementById("net_up").innerHTML = Math.trunc(up / 1000).toFixed(0) + " kbps";
    } else if (up < 1000000000) {
        document.getElementById("net_up").innerHTML = Math.trunc(up / 1000000).toFixed(0) + " mbps";
    } else {
        document.getElementById("net_up").innerHTML = Math.trunc(up / 1000000000).toFixed(0) + " gbps";
    }
    if (dn < 1000) {
        document.getElementById("net_down").innerHTML = jso["net_down"] + " bps";
    } else if(dn < 1000000) {
        document.getElementById("net_down").innerHTML = Math.trunc(dn / 1000) + " kbps";
    } else if (dn < 1000000000) {
        document.getElementById("net_down").innerHTML = Math.trunc(dn / 1000000).toFixed(0) + " mbps";
    } else {
        document.getElementById("net_down").innerHTML = Math.trunc(dn / 1000000000).toFixed(0) + " mbps";
    }

    console.log(up);

    document.getElementById("die_temp").innerHTML = Math.trunc(die_tmp) + " °";
    document.getElementById("die_temp_bar").style.width = Math.trunc(die_tmp) + "%";
    document.getElementById("fan_temp").innerHTML = Math.trunc(fan_tmp) + " °";
    document.getElementById("fan_temp_bar").style.width = Math.trunc(fan_tmp) + "%";

    let mem_used = mem_total - mem_free;

    update_cpus(cpu);
    update_cpu(cpu_avg);
    update_mem(mem_free, mem_used);
    update_net(up, dn);

    let arr = jso["procs"];

    $("#tb").empty();

    for (var i = 0; i < 300; i++) {
        let row = table.insertRow();
        let c1 = row.insertCell(0);
        let c2 = row.insertCell(1);
        let c3 = row.insertCell(2);
        let c4 = row.insertCell(3);
        let c5 = row.insertCell(4);
        let c6 = row.insertCell(5)
        c1.innerHTML = arr[i][0];
        c2.innerHTML = arr[i][1];
        c3.innerHTML = arr[i][2];
        c4.innerHTML = arr[i][3];
        let state = arr[i][5];

        if(state === "UninterruptibleDiskSleep") {
            state = "UDS";
        }

        c6.innerHTML = state;

        if (arr[i][4] > 1000) {
            c5.innerHTML = (arr[i][4] / 1000).toFixed(2) + "G"
        } else {
            c5.innerHTML = arr[i][4] + "M";
        }

        let rowClickHandler = function(r) {
            return function() {
                var pid = r.cells[0].innerHTML;
                var name = r.cells[1].innerHTML;
                var status = r.cells[5].innerHTML;
                let new_proc = pid + " " + name;
                let play = document.getElementById("play");
                let proc_tag = document.getElementById("program-name");
                btns = document.getElementById("btns-div");
                console.log("hello");

                if (proc_tag.innerHTML === new_proc) {
                    proc_tag.classList.remove("visible");
                    proc_tag.classList.add("invisible");
                    btns.classList.remove("visible");
                    btns.classList.add("invisible");
                    proc_tag.innerHTML = "";
                    return;
                }
                proc_tag.classList.remove("invisible");
                proc_tag.classList.add("visible");
                btns.classList.remove("invisible");
                btns.classList.add("visible");
                proc_tag.innerHTML = pid + " " + name;
                if (status === "Sleeping") {
                    if (play.classList.contains("fa-play")) {
                        play.classList.remove("fa-play");
                        play.classList.remove("text-success");
                        play.classList.add("fa-pause");
                        play.classList.add("text-light");
                    }
                } else {
                    if (play.classList.contains("fa-pause")) {
                        play.classList.remove("fa-pause");
                        play.classList.remove("text-light");
                        play.classList.add("fa-play");
                        play.classList.add("text-success");
                    }
                }



                let stp = function(pid) {
                    return function() {
                        console.log(pid)
                        let json = '{"kill":' + pid + '}';
                        cpuWs.send(json);
                        proc_tag.classList.remove("visible");
                        proc_tag.classList.add("invisible");
                        btns.classList.remove("visible");
                        btns.classList.add("invisible");
                        proc_tag.innerHTML = "";
                    }
                }
                document.getElementById("stp").onclick = stp(pid);
                proc_tag.onclick = function() {
                    proc_tag.classList.remove("visible");
                    proc_tag.classList.add("invisible");
                    btns.classList.remove("visible");
                    btns.classList.add("invisible");
                    proc_tag.innerHTML = "";
                }};
        };
        row.onclick = rowClickHandler(row);
    }
}


let x = null;

cpuWs.onopen = () => {
    setInterval(function() {cpuWs.close()}, 3600000);
}

cpuWs.onmessage = (ev) => {
    let json = JSON.parse(ev.data);
    if (json["disk_total"] != undefined) {
        let secs = json["up"];
        let disk_total = json["disk_total"];
        let disk_free = json["disk_free"];
        let disk_used = disk_total - disk_free;
        let disk_percent = disk_used / disk_total * 100;
        let percent = secs / 604800 * 100;

        document.getElementById("count").innerHTML = json["ms"];
        document.getElementById('uptime').innerHTML = Math.trunc(secs / 60 / 60 / 24) + "D " + Math.trunc(secs / 60 / 60 % 24) + "H " + Math.trunc(secs / 60 % 60) + "M";
        document.getElementById("uptime_bar").style.width = percent + "%";
        document.getElementById("disk_free").innerHTML = disk_free + " GB";
        document.getElementById("disk_bar").style.width = disk_percent + "%";
        document.getElementById("filesystem").innerHTML = disk_used + "GB";
        document.getElementById("disks").innerHTML = disk_total + "GB";
    } else if (json["ms"] != undefined) {
        document.getElementById("count").innerHTML = json["ms"];
        console.log(json["ms"]);
    } else if (json["cores"] != undefined) {
        json_update(json);
        console.log("cores");
    }
};

const cpuAvgCanvas = document.getElementById("cpu");
var cpuAvgChart = new Chart(cpuAvgCanvas, {
    type: "line",
    options: {
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    display: true,
                    gridLines: {
                        color: "transparent",
                    },
                },
            ],
            yAxes: [
                {
                    display: true,
                    gridLines: {
                        color: "transparent",
                    },
                    ticks: {
                        min: 0,
                    },
                },
            ],
        },
    },
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],
        datasets: [{
            data: [6, 7, 9, 7, 1, 3, 4, 2, 5, 10],
            borderColor:'#8b60c9',
            label: 'Core Usage Percent',
            backgroundColor: "transparent",
            pointRadius: 0,
            borderWidth: 2,
            backgroundColor: "#8b60c9",
        }],
    },
});

let inner = document.getElementById("avg_title");

function update_cpu(b) {
    cpuAvgChart.data.datasets[0].data.push(b);

    document.getElementById("avg_title").innerHTML = b + "%";
    document.getElementById("avg_pb").style.width = b + "%";

    cpuAvgChart.data.datasets[0].data.shift();
    cpuAvgChart.update();
}




var cpusCanvas = document.getElementById("cpus");
var cpusChart = new Chart(cpusCanvas, {
    type: 'bar',
    data: {
        labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
        datasets: [{
            data: [],
            borderWidth: 1,
            borderColor:'#8b60c9',
            label: 'Core Usage Percent',
            backgroundColor: "#8b60c9",
        }]
    },
    options: {
        responsive: true,
        maintainAspectRatio: true,
        title: {
            display: true,
        },
        legend: {
            display: false,
        },
        scales: {
            xAxes: [
                {
                    display: true,
                    gridLines: {
                        color: "transparent",
                    },
                },
            ],
            yAxes: [
                {
                    display: true,
                    gridLines: {
                        color: "transparent",
                    },
                },
            ],
        },
    },
});


function update_cpus(v) {
    cpusChart.data.datasets[0].data = v;
    let x = [];

    for (let i=0; i < v.length; i++) {
        x[i] = "Cpu" + (i + 1);
    }

    cpusChart.data.labels = x
    cpusChart.update();
}

const memCanvas = document.getElementById("mem");
var memChart = new Chart(memCanvas, {
    type: "doughnut",
    options: {
        cutoutPercentage: 90,
        legend: {
            display: false,
        },
    },
    data: {
        labels: ["Free", "Used"],
        datasets: [
            {
                data: [300, 50, 100, 60],
                borderWidth: [0, 0, 0, 0],
                backgroundColor: ["rgba(75, 75, 75, 0.7)", "rgba(238, 139, 152, 0.7)"],
            },
        ],
    },
});

function update_mem(f, u) {
    memChart.data.datasets[0].data = [f, u];
    document.getElementById("mem_cl").innerHTML = (f / 1000).toFixed(2) + " GB";
    memChart.update();
}

const netCanvas = document.getElementById("lineChart1");
var netChart = new Chart(netCanvas, {
    type: "line",
    options: {
        scales: {
            xAxes: [
                {
                    display: true,
                    gridLines: {
                        display: false,
                    },
                },
            ],
            yAxes: [
                {
                    display: true,
                    gridLines: {
                        display: false,
                    },
                    type: 'logarithmic',
                },
            ],
        },
        legend: {
            display: false,
        },
    },
    data: {
        labels: [6, 7, 9, 7, 1, 3, 4, 2, 5, 10],
        datasets: [
            {
                label: "Team Drills",
                fill: true,
                lineTension: 0.3,
                backgroundColor: "transparent",
                borderColor: "#67c77d",
                pointBorderColor: "#67c77d",
                pointHoverBackgroundColor: "#EF8C99",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                borderWidth: 2,
                pointBackgroundColor: "#EF8C99",
                pointBorderWidth: 2,
                pointHoverRadius: 4,
                pointHoverBorderColor: "#fff",
                pointHoverBorderWidth: 0,
                pointRadius: 0,
                pointHitRadius: 0,
                data: [6, 7, 9, 7, 1, 3, 4, 2, 5, 10],
                spanGaps: true,
                backgroundColor: "#67c77d",

            },
            {
                label: "Team Drills",
                fill: true,
                lineTension: 0.3,
                backgroundColor: "transparent",
                borderColor: "#cc525e",
                pointBorderColor: "#cc525e",
                pointHoverBackgroundColor: "rgba(238, 139, 152, 0.24)",
                borderCapStyle: "butt",
                borderDash: [],
                borderDashOffset: 0.0,
                borderJoinStyle: "miter",
                borderWidth: 2,
                pointBackgroundColor: "rgba(238, 139, 152, 0.24)",
                pointBorderWidth: 2,
                pointHoverRadius: 4,
                pointHoverBorderColor: "#fff",
                pointHoverBorderWidth: 0,
                pointRadius: 0,
                pointHitRadius: 0,
                data: [10, 5, 2, 9, 1, 3, 4, 7, 7, 6],
                spanGaps: true,
                backgroundColor: "#cc525e",

            },
        ],
    },
});

function update_net(up, dn) {
    netChart.data.datasets[1].data.push(up.toFixed(0));
    netChart.data.datasets[0].data.push(dn.toFixed(0));

    netChart.data.datasets[1].data.shift();
    netChart.data.datasets[0].data.shift();

    netChart.update();
}
